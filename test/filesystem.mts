/**
 * SPDX-PackageName: kwaeri/filesystem
 * SPDX-PackageVersion: 0.9.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'

// INCLUDES
import * as assert from 'assert';
import { Filesystem } from '../src/filesystem.mjs';
import debug from 'debug';


// DEFINES
const filesystem = new Filesystem();

/* Configure Debug module support */
const DEBUG = debug( 'kue:filesystem-test' );


// SANITY CHECK - Makes sure our tests are working proerly
describe(
    'PREREQUISITE',
    () => {
        describe(
            'Sanity Test(s)',
            () => {

                it(
                    'Should return true.',
                    () => {
                        assert.equal( [1,2,3,4].indexOf(4), 3 );
                    }
                );

            }
        );
    }
);


describe(
    'Filesystem Functionality Test Suite',
    () => {


        describe(
            'Get Path to Current User Directory Test',
            () => {
                it(
                    `Should return the path to the current logged in user's home/profile directory.`,
                    () => {
                        DEBUG( `Path to current users directory: '${Filesystem.getPathToCUD()}'`);

                        assert.notEqual( Filesystem.getPathToCUD(), "" );
                    }
                );
            }
        );


        describe(
            'Get Path to System Conf Directory Test',
            () => {
                it(
                    `Should return the path to the system conf directory.`,
                    () => {
                        DEBUG( `Path to system conf directory: '${Filesystem.getPathToSCD()}'`);

                        assert.notEqual( Filesystem.getPathToSCD(), "" );
                    }
                );
            }
        );


        describe(
            'Create Directory Test [I. In CWD]',
            () => {
                it(
                    `Should create the directory "./tmp".`,
                    async () => {
                        try {
                            return Promise.resolve(
                                assert.equal(
                                    await filesystem.createDirectory( Filesystem.getPathToCWD() + '/tmp' ),
                                    true
                                )
                            );
                        }
                        catch( exception ) {
                            DEBUG( exception );
                        }
                    }
                );
            }
        );


        describe(
            'Create Directory Test [II. A subdir of first in `tmp`]',
            () => {
                it(
                    `Should create the directory "./tmp/sub".`,
                    async () => {
                        try {
                            return Promise.resolve(
                                assert.equal(
                                    await filesystem.createDirectory( Filesystem.getPathToCWD() + '/tmp/sub' ),
                                    true
                                )
                            );
                        }
                        catch( exception ) {
                            DEBUG( exception );
                        }
                    }
                );
            }
        );


        describe(
            'Create File Test [I. In `tmp`.]',
            () => {
                it(
                    `Should create the file "./tmp/tmpFile".`,
                    async () => {
                        try {
                            return Promise.resolve(
                                assert.equal(
                                    await filesystem.createFile( Filesystem.getPathToCWD() + '/tmp', "tmpFile", "Test file contents" ),
                                    true
                                )
                            );
                        }
                        catch( exception ) {
                            DEBUG( exception );
                        }
                    }
                );
            }
        );


        describe(
            'Create File Test [II. In subdir `tmp/sub`.]',
            () => {
                it(
                    `Should create the file "./tmp/sub/tmpSubFile".`,
                    async () => {
                        try {
                            return Promise.resolve(
                                assert.equal(
                                    await filesystem.createFile( Filesystem.getPathToCWD() + '/tmp/sub', "tmpSubFile", "Test sub file contents" ),
                                    true
                                )
                            );
                        }
                        catch( exception ) {
                            DEBUG( exception );
                        }
                    }
                );
            }
        );


        describe(
            'Copy Test [I. Recursive, `tmp/sub`.]',
            () => {
                it(
                    `Should RECURSIVELY copy the directory "./tmp/sub" to "./tmp/subCopy".`,
                    async () => {
                        const tmpSourcePath = Filesystem.getPathToCWD() + '/tmp/sub',
                            tmpDestinationPath = Filesystem.getPathToCWD() + '/tmp/subCopy';

                        try {
                            const result = await filesystem.copy( tmpSourcePath, tmpDestinationPath, true );
                            return Promise.resolve(
                                assert.equal(
                                    result,
                                    true
                                )
                            );
                        }
                        catch( exception ) {
                            DEBUG( exception );
                        }
                    }
                );
            }
        );


        describe(
            'Copy Test [II. Non-recursive, `tmp/sub`.]',
            () => {
                it(
                    `Should ONLY copy the directory "./tmp/sub" to "./tmp/subCopy2".`,
                    async () => {
                        const tmpSourcePath = Filesystem.getPathToCWD() + '/tmp/sub',
                            tmpDestinationPath = Filesystem.getPathToCWD() + '/tmp/subCopy2';

                        try {
                            const result = await filesystem.copy( tmpSourcePath, tmpDestinationPath );
                            return Promise.resolve(
                                assert.equal(
                                    result,
                                    true
                                )
                            );
                        }
                        catch( exception ) {
                            DEBUG( exception );
                        }
                    }
                );
            }
        );


        describe(
            'Copy Test [III. File `tmp/tmpFile`.]',
            () => {
                it(
                    `Should copy the file "./tmp/tmpFile" to "./tmp/tmpFileCopy".`,
                    async () => {
                        const tmpSourcePath = Filesystem.getPathToCWD() + '/tmp/tmpFile',
                            tmpDestinationPath = Filesystem.getPathToCWD() + '/tmp/tmpFileCopy';

                        try {
                            const result = await filesystem.copy( tmpSourcePath, tmpDestinationPath );
                            return Promise.resolve(
                                assert.equal(
                                    result,
                                    true
                                )
                            );
                        }
                        catch( exception ) {
                            DEBUG( exception );
                        }
                    }
                );
            }
        );


        describe(
            'Move Test [I. Directory, `tmp/subCopy`.]',
            () => {
                it(
                    `Should move the directory "./tmp/subCopy" to "./tmp/movedCopy".`,
                    async () => {
                        const tmpSourcePath = Filesystem.getPathToCWD() + '/tmp/subCopy',
                            tmpDestinationPath = Filesystem.getPathToCWD() + '/tmp/movedCopy';

                        try {
                            return Promise.resolve(
                                assert.equal(
                                    await filesystem.move( tmpSourcePath, tmpDestinationPath ),
                                    true
                                )
                            );
                        }
                        catch( exception ) {
                            DEBUG( exception );
                        }
                    }
                );
            }
        );


        describe(
            'Move Test [II. File, `tmp/tmpFileCopy`.]',
            () => {
                it(
                    `Should move the file "./tmp/tmpFileCopy" to "./tmp/movedTmpFile".`,
                    async () => {
                        const tmpSourcePath = Filesystem.getPathToCWD() + '/tmp/tmpFileCopy',
                            tmpDestinationPath = Filesystem.getPathToCWD() + '/tmp/movedTmpFile';

                        try {
                            return Promise.resolve(
                                assert.equal(
                                    await filesystem.move( tmpSourcePath, tmpDestinationPath ),
                                    true
                                )
                            );
                        }
                        catch( exception ) {
                            DEBUG( exception );
                        }
                    }
                );
            }
        );


        describe(
            'Delete Test [Delete File]',
            () => {
                it(
                    `Should delete the file "./tmp/tmpFile".`,
                    async () => {
                        try {
                            return Promise.resolve(
                                assert.equal(
                                    await filesystem.delete( Filesystem.getPathToCWD() + '/tmp/tmpFile' ),
                                    true
                                )
                            );
                        }
                        catch( exception ) {
                            DEBUG( exception );
                        }
                    }
                );
            }
        );


        describe(
            'Remove Diretory Test [Delete Diretory]',
            () => {
                it(
                    `Should delete the directory "./tmp".`,
                    async () => {
                        try {
                            return Promise.resolve(
                                assert.equal(
                                    await filesystem.removeDirectory( Filesystem.getPathToCWD() + '/tmp', true ),
                                    false
                                )
                            );
                        }
                        catch( exception ) {
                            DEBUG( exception );
                        }
                    }
                );
            }
        );


        describe(
            'Remove Test [Recursive, Delete Directory.]',
            () => {
                it(
                    `Should delete the directory "./tmp".`,
                    async () => {
                        try {
                            if( await filesystem.createFile( Filesystem.getPathToCWD() + '/tmp', "tmpFile", "Test file contents" ) )
                                return Promise.resolve(
                                    assert.equal(
                                        await filesystem.remove( Filesystem.getPathToCWD() + '/tmp', true, true ),
                                        true
                                    )
                                );
                        }
                        catch( exception ) {
                            DEBUG( exception );
                        }
                    }
                );
            }
        );


    }
);
