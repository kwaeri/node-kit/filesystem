/**
 * SPDX-PackageName: kwaeri/filesystem
 * SPDX-PackageVersion: 0.9.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


 'use strict'


 // INCLUDES

 // ESM WRAPPER
export {
    Filesystem
} from './src/filesystem.mjs'

export type {
    FilesystemEntry,
    FilesystemRecord
} from './src/filesystem.mjs'
