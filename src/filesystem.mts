/**
 * SPDX-PackageName: kwaeri/filesystem
 * SPDX-PackageVersion: 0.9.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'


// INCLUDES
import * as os from 'os';
import * as fs from 'node:fs';
import * as _fs from 'node:fs/promises';
import * as _path from 'path';
import debug from 'debug';


/* Configure Debug module support */
const DEBUG = debug( 'kue:filesystem' );


// DEFINES


export type FilesystemEntry = Record<string, FilesystemRecord>;

export type FilesystemRecord = {
    type: string|null;
    children?: FilesystemEntry|null|undefined;
}


export interface BaseFilesystem {
    exists( path: string ): Promise<boolean>;
    createDirectory( directoryPath: string ): Promise<boolean|string>;
    createFile( path: string, file: string, content: string ): Promise<boolean>;
    copy( path: string, destination: string, recursive: boolean ): Promise<boolean>;
    move( path: string, destination: string ): Promise<boolean>;
    delete( path: string ): Promise<boolean>;
    removeDirectory( path: string ): Promise<boolean>;
    remove( path: string ): Promise<boolean>;
}


export interface BaseDescriber {
    list( path: string ): Promise<fs.Dirent[]>;
    getDirectoryStructure( path: fs.PathLike ): Promise<FilesystemEntry|null>
}


/**
 * The Filesystem Class
 *
 * The {Filesystem} class implements the { BaseFilesystem }, as well as the
 * { BaseDescriber } interface - and allows to manipulate the filesystem
 * with greater control using less implementation.
 */
export class Filesystem implements BaseDescriber, BaseFilesystem {


    /**
     * Method to ensure a relative path is converted into an absolute path
     *
     * @param { fs.PathLike } path The path in question
     *
     * @returns {fs.PathLike } a `fs.PathLike`
     */
    async resolvePath( path: fs.PathLike ) : Promise<fs.PathLike> {
        let realPath = path.toString();

        try {
            // Check for relative path
            if( realPath.charAt( 0 ) == "." )
                realPath = await _fs.realpath( "." ) + "/" + realPath.substring( 1, realPath.length );

            return Promise.resolve( realPath as fs.PathLike );
        }
        catch( exception ) {
            return Promise.reject( exception );
        }
    }


    /**
     * Method which asynchronously checks if a path, or file/folder exists
     *
     * @param { fs.PathLike } path A path, or the path to the directory or file being checked for existence
     *
     * @returns { Promise<boolean> } a promise of a `boolean`
     */
    public async exists( path: fs.PathLike ): Promise<boolean> {
        try {
            const feedback = await _fs.access( await this.resolvePath( path ), fs.constants.F_OK );

            return Promise.resolve( feedback === undefined ) as any;
        }
        catch( exception ) {
            DEBUG( `[exists] ${exception}` );

            return Promise.resolve( false );
        }
    }


    /**
     * Method which asynchronously lists the contents of a directory
     *
     * @param { fs.PathLike } path A path to the directory contents will be listed from.
     *
     * @returns { Promise<fs.Dirent[]> } a promise of a `fs.Dirent[]`
     */
    public async list( path: fs.PathLike ): Promise<fs.Dirent[]> {
        try {
            await _fs.access( await this.resolvePath( path ), fs.constants.F_OK );

            return Promise.resolve( await _fs.readdir( path, { withFileTypes: true } ) );
        }
        catch( exception ) {
            return Promise.reject( exception );
        }
    }


    /**
     * Method which recursively get's a list of resources from a path, stacked in an object as it exists
     * on disk.
     *
     * @param { fs.PathLike } path The path to the filesystem that a representation is being requested for
     *
     * @returns { Promise<FilesystemEntry|null> } a promise of a `FilesystemEntry` or `null`
     */
    public async getDirectoryStructure( path: fs.PathLike ): Promise<FilesystemEntry|null> {
        let listing,
            directory: FilesystemEntry = {},
            realPath: fs.PathLike;

        DEBUG( `[FILESYSTEM][GET_DIR_STRUCTURE] Get directory structure for '${path}'` );

        try {
            if( await this.exists( path ) ) {
                realPath = await this.resolvePath( path );

                DEBUG( `[FILESYSTEM][GET_DIR_STRUCTURE] Listing for real path ''${realPath}': ` );

                listing = await this.list( realPath );

                DEBUG( listing );

                await Promise.all(
                    listing.map(
                        async ( item: any, i: number ) => {
                            DEBUG( `[FILESYSTEM][GET_DIR_STRUCTURE] Mapping ${typeof item} item '${i}' ⇨ '${item.name}'` );

                            directory[item.name] = { type: null, children: null };

                            if( item.isDirectory() ) {
                                directory[item.name].type = "directory";
                                directory[item.name].children = await this.getDirectoryStructure( _path.join( realPath.toString(), item.name ) ) || null;
                            }
                            else {
                                directory[item.name].type = ( item.isFile() ) ? 'file' :
                                    ( ( item.isSymbolicLInk() ) ? 'symbolic' : ( await this.getFileInformation( _path.join( realPath.toString(), item.name ) ) ).toString() )
                            }
                        }
                    )
                );

                return Promise.resolve( directory );
            }

            return Promise.resolve( null );
        }
        catch( exception ) {
            return Promise.reject( exception );
        }

        //return Promise.resolve( <T>{ type: 'directoryStructure', result: true, files: children, path: path } );
    };


    /**
     * Method to resolve type from file statistics so it can be stored in string form
     *
     * @param { fs.Stats } stats File statistics for a file
     *
     * @returns { string } a `String`
     */
    public getFilesystemType( stats: fs.Stats ): String {
        if( stats.isFile() )
            return 'file';

        if( stats.isDirectory() )
            return 'directory';

        if( stats.isSymbolicLink() )
            return 'symbolic';

        if( stats.isSocket() )
            return 'socket';

        if( stats.isBlockDevice() )
            return 'block';

        if( stats.isCharacterDevice() )
            return 'character';

        if( stats.isFIFO() )
            return 'fifo';


        return 'UNKNOWN';
    }


    /**
     * Method which get's information about a path. Will reject the promise if the path does not exist.
     *
     * @param { fs.PathLike } path The path that file system information is being fetched for
     *
     * @returns { Promise<String> } a promise of a `String`
     */
    public async getFileInformation( path: fs.PathLike ): Promise<String> {
        let type;
        try {
            await _fs.access( await this.resolvePath( path ), fs.constants.F_OK );

            return Promise.resolve( this.getFilesystemType( await _fs.lstat( await this.resolvePath( path ) ) ) );
        }
        catch( exception ) {
            return Promise.reject( exception );
        }
    }


    /**
     * Method that copies a source file or folder to the specified destination
     *
     * @param { fs.PathLike } path The source path
     * @param { fs.PathLike } destination The destination path
     * @param { boolean } recursive Whether to copy sub-files & folders when path is a directory
     *
     * @returns { Promise<boolean> } a promise of a `boolean`
     */
    public async copy( source: fs.PathLike, destination: fs.PathLike, recursive: boolean = false ): Promise<boolean> {
        try {
            // If bot paths are valid  (exist, no exist, re spectively)
            if( await this.exists( source ) && !( await this.exists( destination ) ) ) {
                // If the source is a directory
                if( await this.getFileInformation( source ) === "directory" ) {
                    // Create the directory at destination
                    await this.createDirectory( destination );

                    if( recursive ) {
                        const items = await this.list( source );

                        DEBUG( items );

                        await Promise.all(
                            items.map(
                                async ( item ) => {
                                    // Create derived paths
                                    const itemPath = _path.join( await this.resolvePath( source ) as string, item.name ),
                                        itemDestination = _path.join( destination as string, item.name );

                                    return item.isDirectory() ?
                                        await this.copy( itemPath, itemDestination, true ) :
                                        await this.copyFile( itemPath, itemDestination );
                                }
                            )
                        );
                    }

                    return Promise.resolve( true );
                }
                else {
                    return Promise.resolve( await this.copyFile( source, destination ) );
                }
            }

            // If we do not copy anything ⇨ ensure we reflect as much:
            return Promise.resolve( false );
        }
        catch( exception ) {
            DEBUG( exception );

            return Promise.resolve( false );
        }
    }


    /**
     * Method that moves a source file or folder to the specified destination
     *
     * @param { fs.PathLike } source The source path
     * @param { fs.PathLike } destination The destination path
     *
     * @returns { Promise<boolean> } a promise of a `boolean`
     */
    public async move( source: fs.PathLike, destination: fs.PathLike ): Promise<boolean> {
        try {
            if( await this.exists( source ) ) {
                // We'll implement move simlarly to how its implemented in posix (i.e. mv source dest...if dest has a trailing slash,
                // we typically put the source into the location specified by - or after - the slash.):
                const target = ( destination as string ).slice( -1 ) === '/' ? _path.join( destination as string, _path.basename( source as string ) ): destination;

                if( await this.copy( source, target, true ) )
                    ( await this.getFileInformation( source ) === "directory" ) ?
                        await this.remove( source, true, true ) : await this.delete( source );

                return Promise.resolve( true );
            }

            return Promise.resolve( false );
        }
        catch( exception ) {
            DEBUG( exception );

            return Promise.resolve( false );
        }
    }


    /**
     * Method which asynchronously creates a directory if it does not already exist
     *
     * @param { fs.PathLike } directoryPath The path, including the directory name, that is to be created
     * @param { boolean } recursive Whether or not to create parent directories
     *
     * @returns { Promise<boolean|string> } a promise of a `boolean` or `string`
     */
    public async createDirectory( path: fs.PathLike, recursive: boolean = false ): Promise<boolean|string> {
        try {
            const result = await _fs.mkdir( await this.resolvePath( path ), { recursive } );

            return Promise.resolve( (  result === undefined ) ? true : result );
        }
        catch( exception ) {
            DEBUG( exception );

            return Promise.resolve( false );
        }
    }


    /**
     * Method which asynchronously creates a file
     *
     * @param { fs.PathLike } path The path to the directory where the file will be created
     * @param { fs.PathLike } file The name of the file to be created
     * @param { string } content The contents for the file to be created
     *
     * @returns { Promise<FilesystemPromise> } a promise of a `boolean`
     */
    public async createFile( path: fs.PathLike, file: fs.PathLike, content: string, base64?: boolean ): Promise<boolean> {
        try {
            if( await this.exists( path ) )
                if( base64 )
                    await _fs.writeFile( _path.join( await this.resolvePath( path ) as string, file as string ), content, 'base64' );
                else
                    await _fs.writeFile( _path.join( await this.resolvePath( path ) as string, file as string ), content );

            return Promise.resolve( true );
        }
        catch( exception ) {
            DEBUG( exception );

            return Promise.resolve( false );
        }
    }


    /**
     * Method to copy a file
     *
     * @param { fs.PathLike } source The string path to the file to copy
     * @param { fs.PathLike } destination The string path for the destination file
     * @param { boolean } allowOverwrite A flag for whether to allow an existing file located at ( destination ) to be overwritten
     *
     * @returns { Promise<boolean> } a promise of a `boolean`
     */
    public async copyFile( source: fs.PathLike, destination: fs.PathLike, allowOverwrite: boolean = false ): Promise<boolean> {
        try {
            await _fs.access( await this.resolvePath( source ), fs.constants.F_OK );

            if( allowOverwrite )
                await _fs.copyFile( await this.resolvePath( source ), destination );
            else
                await _fs.copyFile( await this.resolvePath( source ), destination, fs.constants.COPYFILE_EXCL );

            return Promise.resolve( true );
        }
        catch( exception ) {
            DEBUG( exception );

            return Promise.resolve( false );
        }
    }


    /**
     * Method to delete a file descriptor
     *
     * @param { fs.PathLike } path The path to the file descriptor to delete
     *
     * @returns { Promise<boolean> } a promise of a `boolean`
     */
    public async delete( path: fs.PathLike ): Promise<boolean> {
        try {
            await _fs.access( await this.resolvePath( path ), fs.constants.F_OK );
            await _fs.unlink( await this.resolvePath( path ) );

            return Promise.resolve( true );
        }
        catch( exception ) {
            DEBUG( exception );

            return Promise.resolve( false );
        }
    }


    /**
     * Method to delete an empty  directory.
     *
     * @deprecated fs.rmdir( path, { recursive: true } ) is deprecated since Node v14.14. See filesystem.remove( path, recursive, force ) instead.
     *
     * @param { fs.PathLike } path The path to the empty directory to delete. Can be relative
     * @param { boolean } recursive A flag for whether to recursively delete empty folders nested within the target directory
     *
     * @returns { Promise<boolean> } a promise of a `boolean`
     *
     * Can throw `ENOENT` or `ENODIR` depending on whether invocation occurs on Windows or Posix, respectively, if `path` is
     * not a directory.
     *
     * To recursively delete a directory and all containing files and folders, use {@link remove}( path, true, true )
     */
    public async removeDirectory( path: fs.PathLike, recursive: boolean = false, force: boolean = false ): Promise<boolean> {
        try {
            await _fs.access( await this.resolvePath( path ), fs.constants.F_OK );

            // Trigger a warning if recursive is set to true:
            if( recursive )
                console.warn( `fs.rmdir( path, { recursive: true } ) is deprecated since Node v14.14. See filesystem.remove( path, recursive, force ) instead.` );

            await _fs.rmdir( await this.resolvePath( path ) );

            return Promise.resolve( true  );
        }
        catch( exception ) {
            DEBUG( exception );

            return Promise.resolve( false );
        }
    }


    /**
     * Method to remove a resource.
     *
     * @param { fs.PathLike } path The path to the resource. Can be relative for
     * @param { boolean } recursive Whether to recursively traverse the filesystem from the pont of entry (at `path`).
     * @param { boolean } force Whether to forecibly remove a resource (i.e. a non-empty directory, by removing its contents first)
     *
     * @returns { Promise<boolean> } a promise of a `boolean`
     */
    public async remove( path: fs.PathLike, recursive: boolean = false, force: boolean = false ): Promise<boolean> {
        try {
            await _fs.access( await this.resolvePath( path ), fs.constants.F_OK );

            await _fs.rm( await this.resolvePath( path ), { recursive, force } );

            return Promise.resolve( true );
        }
        catch( exception ) {
            DEBUG( exception );

            return Promise.resolve( false );
        }
    }


    /**
     * A convenience function for sorting a list of alphanumeric strings
     *
     * @param { string } a The string being compared to
     * @param { string } b The string being compared
     *
     * @returns { Number }
     */
    public alphaNumericSort( a: string, b: string ) {
        a = typeof a === 'string' ? a.toLowerCase() : ( a as any ).toString();
        b = typeof b === 'string' ? b.toLowerCase() : ( b as any ).toString();

        return a.localeCompare( b );
    }


    /**
     * Takes a 'camelCasedANDDecorated' string, optionally breaks it apart at each capital letter -
     * while preserving acronymns - using hyphens, removes special characters, and makes the string
     * lower-case so that it is safe to use as a file name.
     *
     * @param { string } name The string that is being made file-safe
     * @param { boolean } stripSpaces Flags whether or not to strip spaces from the name. Defaults to true.
     * @param { boolean } dontBreak Flags whether not to break the string at each capitol letter, using hyphens. Defaults to false (breaks).
     *
     * @returns { string }
     */
    static getFileSafeName( name: string, stripSpaces: boolean = true, dontBreak: boolean = false ): string {
        let fileSafeName = '';

        // First, strip spaces - we don't want these!
        if( stripSpaces )
            fileSafeName = name.replace( /\s+/g, '' )

        if( !dontBreak )
            // This line separates words at each capital letter - and properly observes acronyms
            // such that sampleStringWITHAcronyms becomes sample-String-WITH-Acronyms:
            fileSafeName = fileSafeName.replace( /(?<!^)([A-Z][a-z]|(?<=[a-z])[A-Z])/mg, '-$1' );

        // Next we must remove illegal characters, such as '/' - and ensure all characters are
        // lowercased. sample-String-WITH-Acronyms becomes sample-string-with-acronyms:
        return fileSafeName.replace( /([/])/, '' ).toLowerCase();
    }


    /**
     * Takes a string which has been transformed to work as a file name, and converts it
     * into a string which is appropriate for use as a variable name.
     *
     * @param { string } name The string that is to be converted for use as a variable or class name
     *
     * @returns { string }
     */
    static getCodeSafeName( name: string ): string {
        // Return the value of our replace regex, which should capitalize each first letter
        // following a hyphen, creating a code safe variable/class name:
        return name.replace(
            /(^|[\s-])\S/g,
            ( match ) => {
                return match.toUpperCase();
            }
        );
    }


    /**
     * Returns the full path to the current working directory.
     *
     * @param none
     *
     * @returns { string }
     */
    static getPathToCWD(): string {
        return fs.realpathSync( '.' );
    }


    /**
     * Returns the path to the current users home (or profile) directory
     *
     * @param none
     *
     * @returns { string }
     */
    static getPathToCUD(): string {
        return os.homedir();
    }


    /**
     * Returns the path to the all-user's (system-wide) configuration directory.
     *
     * @param none
     *
     * @returns { string }
     */
    static getPathToSCD(): string {
        // Returns the all-users-profile directory for Node, /etc for linux, and nothing for MAC :P
        return process.env.ALLUSERSPROFILE || ( process.platform == 'darwin' ? "" : "/etc" );
    }
}

